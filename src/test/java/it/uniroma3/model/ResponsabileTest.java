package it.uniroma3.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ResponsabileTest {

	private Responsabile responsabileTest;
	
	@Before
	public void setUp() throws Exception {
		responsabileTest = new Responsabile("Matteo", "user2", "email2@email.com", "password2", null);
	}
	
	@Test
	public void testGetInstance() {
		Responsabile responsabileIdentico = new Responsabile("Matteo", "user2", "email2@email.com", "password2", null);
		assertEquals(responsabileIdentico, responsabileTest);
	}

}
