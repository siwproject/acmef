package it.uniroma3.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.model.Allievo.Sesso;

public class AllievoTest {
	
	private Allievo allievoTest;
	
	@Before
	public void setUp() throws Exception{
		allievoTest = new Allievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, LocalDate.of(2018, Month.JANUARY, 4), "Roma");
	}
	
	@Test
	public void testAggiungiAttivita() {
		assertNotNull(allievoTest);
		Attivita attivita = new Attivita("Attivita di test", LocalDate.of(2018, Month.JANUARY, 4), LocalTime.of(10, 10));
		allievoTest.aggiungiAttivita(attivita);
		assertEquals(attivita, allievoTest.getAttivita().get(0));
	}

	@Test
	public void testEliminaAttivita() {
		Attivita attivita = new Attivita("Attivita di test", LocalDate.of(2018, Month.JANUARY, 4), LocalTime.of(10, 10));
		allievoTest.aggiungiAttivita(attivita);
		assertEquals(attivita, allievoTest.getAttivita().get(0));
		allievoTest.eliminaAttivita(attivita);
		assertEquals(0, allievoTest.getAttivita().size());
	}

	@Test
	public void testEqualsObject() {
		Allievo allievoIdentico = new Allievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, LocalDate.of(2018, Month.JANUARY, 4), "Roma");
		assertEquals(allievoTest, allievoIdentico);
	}

}
