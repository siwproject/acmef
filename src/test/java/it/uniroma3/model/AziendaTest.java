package it.uniroma3.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.model.Allievo.Sesso;

public class AziendaTest {
	
	private Azienda aziendaTest;

	@Before
	public void setUp() throws Exception {
		aziendaTest = Azienda.getInstance();
	}

	@Test
	public void testGetInstance() {
		assertNotNull(aziendaTest);
	}

	@Test
	public void testIscriviAllievo() {
		Allievo allievoMatching = new Allievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, LocalDate.of(2018, Month.JANUARY, 4), "Roma");
		aziendaTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		assertEquals(1, aziendaTest.getAllievi().size());
		assertEquals(allievoMatching, aziendaTest.getAllievo(allievoMatching.getCodiceStudente()));
	}
	
	@Test
	public void testGetAttivita() {
		
		aziendaTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		Allievo allievoIscritto = aziendaTest.getAllievo(10L);
		
		Attivita attivita = new Attivita("Attivita di test", LocalDate.of(2018, Month.JANUARY, 4), LocalTime.of(10, 10));
		allievoIscritto.aggiungiAttivita(attivita);
		
		assertEquals(1, aziendaTest.getAttivita(10L).size());
		assertEquals(attivita, aziendaTest.getAttivita(10L).get(0));
		
	}

}
