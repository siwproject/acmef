package it.uniroma3.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.model.Allievo.Sesso;

public class CasiDusoTest {
	
	private Allievo allievoMatching;
	private Attivita attivitaMatching;
	private CentroDiFormazione centroTest;
	private Responsabile responsabileTest;

	@Before
	public void setUp() throws Exception {
		allievoMatching = new Allievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, LocalDate.of(2018, Month.JANUARY, 4), "Roma");
		attivitaMatching = new Attivita("Attivita di test", LocalDate.of(2018, Month.JANUARY, 4), LocalTime.of(10, 10));
		centroTest = new CentroDiFormazione("CentroTest", "Via della Vasca Navale 3", "0666456789", 100);
		responsabileTest = new Responsabile("Matteo", "user2", "email2@email.com", "password2", centroTest.getId().toString());
	}

	@Test
	public void testUseCase_1_creaNuovaAttivita() {
		Attivita primaAttivitaCreata = centroTest.creaNuovaAttivita("Attivita di Test", "06/14/2018 12:00");
		Attivita secondaAttivitaCreata = centroTest.creaNuovaAttivita("Seconda Attivita di Test", "06/15/2018 12:00");
		
		// Check sulle attivita del centro
		assertEquals(new Attivita("Attivita di Test", "06/14/2018 12:00"), primaAttivitaCreata);
		assertEquals(2, centroTest.getElencoAttivita().size());
		assertEquals(secondaAttivitaCreata, centroTest.getAttivita().get("Seconda Attivita di Test"));
		
		// Check sul centro delle attività
		assertEquals(centroTest, primaAttivitaCreata.getCentro());
		assertEquals(centroTest, secondaAttivitaCreata.getCentro());
	}
	
	@Test
	public void testUseCase_2_iscriviAllievo() {
		Allievo allievoIscritto = centroTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		
		// Check su azienda
		Azienda azienda = centroTest.getAzienda();
		assertEquals(allievoMatching, azienda.getAllievo(10L));
		
		// Check su centro
		assertEquals(allievoMatching, allievoIscritto);
	}
	
	@Test
	public void testUseCase_3_iscriviAdAttivita() {
		
		//--------------- Check se attivita non esistente ---------------------//
		//-------- Non deve essere possibile iscrivere ad attivita ------------//
		try {
			centroTest.iscriviAdAttivita("Attivita Non Esistente", 10L);
		} catch (NullPointerException e) {
			assertTrue(true);
		}
		
		//---------------- Check se allievo non è isritto ---------------------//
		//-------- Non deve essere possibile iscrivere ad attivita ------------//
		
		try {
			centroTest.iscriviAdAttivita("Attivita di Test", 10L);
		} catch (NullPointerException e) {
			assertTrue(true);
		}
		
		//---------------- CASO D'USO di successo ---------------------//
		Attivita attivitaCreata = centroTest.creaNuovaAttivita("Attivita di Test", "06/14/2018 12:00");
		Allievo allievoIscritto = centroTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		
		centroTest.iscriviAdAttivita("Attivita di Test", 10L);
		
		assertEquals(allievoIscritto, attivitaCreata.getAllievi().get(10L));
		assertEquals(attivitaCreata, allievoIscritto.getAttivita().get(0));
		
	}
	
	/**
	 * Per eseguire decommentare nel costruttore di allievo le opportune istruzioni
	 * 
	 */
	@Test
	public void testUseCase_4_vediAllieviPerAttivita() {
		
		// Creazione centri e collegamento con Azienda
		CentroDiFormazione centro1 = new CentroDiFormazione("Centro1", "Via Centro1", "3647863574", 100, responsabileTest);
		CentroDiFormazione centro2 = new CentroDiFormazione("Centro2", "Via Centro2", "3938748954", 200, responsabileTest);
		
		List<CentroDiFormazione> listaCentri = new LinkedList<CentroDiFormazione>();
		listaCentri.add(centro1);
		listaCentri.add(centro2);
		
		Azienda.getInstance().setCentri(listaCentri);
		
		// Creazione Attivita
		Attivita attivita1 = centro1.creaNuovaAttivita("Attivita 1 centro 1", "06/14/2018 12:00");
		Attivita attivita2 = centro1.creaNuovaAttivita("Attivita 2 centro 1", "06/14/2018 12:00");
		Attivita attivita3 = centro2.creaNuovaAttivita("Attivita 1 centro 2", "06/14/2018 12:00");
		Attivita attivita4 = centro2.creaNuovaAttivita("Attivita 2 centro 2", "06/14/2018 12:00");
		
		// Iscrizione allievi ad azienda
		Allievo allievo1 = centro1.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		Allievo allievo2 = centro2.iscriviAllievo("Luca", "Carloni", "lucacarloni2@gmail.com", "8478958743", Sesso.MASCHIO, "06/05/1995", "Anzio");
		Allievo allievo3 = centro1.iscriviAllievo("Mario", "Rossi", "mariorossi3@gmail.com", "3738476478", Sesso.MASCHIO, "11/06/1994", "Viterbo");
		
		
		// Iscrizione allievi ad attivita
		centro1.iscriviAdAttivita("Attivita 1 centro 1", allievo1.getCodiceStudente());
		centro1.iscriviAdAttivita("Attivita 1 centro 1", allievo2.getCodiceStudente());
		
		centro2.iscriviAdAttivita("Attivita 1 centro 2", allievo1.getCodiceStudente());
		centro2.iscriviAdAttivita("Attivita 1 centro 2", allievo2.getCodiceStudente());
		centro2.iscriviAdAttivita("Attivita 1 centro 2", allievo3.getCodiceStudente());
		
		centro2.iscriviAdAttivita("Attivita 2 centro 2", allievo1.getCodiceStudente());
		
		// Esecuzione di caso d'uso
		Map<String, Map<String, List<Allievo>>> report = Azienda.getInstance().elencoAllieviPerAttivita();
		
		// Test su size
		assertEquals(2, report.size());
		assertEquals(2, report.get("Centro1").size());
		assertEquals(2, report.get("Centro2").size());
		assertEquals(2, report.get("Centro1").get("Attivita 1 centro 1").size());
		assertEquals(3, report.get("Centro2").get("Attivita 1 centro 2").size());
		assertEquals(1, report.get("Centro2").get("Attivita 2 centro 2").size());
		
		// Test su oggetti
		assertTrue(report.containsKey(centro1.getNome()));
		assertTrue(report.containsKey(centro2.getNome()));
		assertTrue(report.get(centro1.getNome()).containsKey(attivita1.getNome()));
		assertTrue(report.get(centro1.getNome()).containsKey(attivita2.getNome()));
		assertTrue(report.get(centro2.getNome()).containsKey(attivita3.getNome()));
		assertTrue(report.get(centro2.getNome()).containsKey(attivita4.getNome()));

		assertTrue(report.get(centro1.getNome()).get(attivita1.getNome()).contains(allievo1));
		assertTrue(report.get(centro1.getNome()).get(attivita1.getNome()).contains(allievo2));
		assertTrue(report.get(centro2.getNome()).get(attivita3.getNome()).contains(allievo1));
		assertTrue(report.get(centro2.getNome()).get(attivita3.getNome()).contains(allievo2));
		assertTrue(report.get(centro2.getNome()).get(attivita3.getNome()).contains(allievo3));
		assertTrue(report.get(centro2.getNome()).get(attivita4.getNome()).contains(allievo1));
	}
	
	@Test
	public void testUseCase_5_disiscriviAllievo() {
		Attivita attivitaCreata = centroTest.creaNuovaAttivita("Attivita di Test", "06/14/2018 12:00");
		Allievo allievoIscritto = centroTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		centroTest.iscriviAdAttivita("Attivita di Test", 10L);
		centroTest.disiscriviAllievo(allievoIscritto.getCodiceStudente(), "Attivita di Test");
		
		// Check che l'allievo rimane iscritto presso l'azienda
		assertTrue(Azienda.getInstance().getAllievi().containsKey(10L));
		
		assertFalse(attivitaCreata.getAllAllievi().contains(allievoIscritto));
		assertFalse(allievoIscritto.getAttivita().contains(attivitaCreata));
	}

}
