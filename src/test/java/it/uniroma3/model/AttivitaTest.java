package it.uniroma3.model;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

import org.junit.*;

import it.uniroma3.model.Allievo.Sesso;

public class AttivitaTest {

	private Attivita attivitaTest;
	private Allievo allievoTest;

	@Before
	public void setUp() throws Exception {
		attivitaTest = new Attivita("Attivita di test", LocalDate.of(2018, Month.JANUARY, 4), LocalTime.of(10, 10));
		allievoTest = new Allievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, LocalDate.of(2018, Month.JANUARY, 4), "Roma");
	}

	@Test
	public void testAggiungiAllievo() {
		assertNotNull(attivitaTest);
		assertNotNull(allievoTest);

		assertEquals(0, attivitaTest.getAllievi().size());

		this.attivitaTest.aggiungiAllievo(allievoTest);

		assertEquals(1, attivitaTest.getAllievi().size());
		assertEquals(allievoTest, attivitaTest.getAllievi().get(allievoTest.getCodiceStudente()));
		
		assertEquals(attivitaTest, allievoTest.getAttivita().get(0));
	}

	@Test
	public void testDisiscriviAllievo() {
		this.attivitaTest.aggiungiAllievo(allievoTest);

		assertEquals(1, attivitaTest.getAllievi().size());
		assertEquals(allievoTest, attivitaTest.getAllievi().get(allievoTest.getCodiceStudente()));

		this.attivitaTest.disiscriviAllievo(allievoTest.getCodiceStudente());
		
		assertEquals(0, attivitaTest.getAllievi().size());
		assertNull(attivitaTest.getAllievi().get(allievoTest.getCodiceStudente()));
		
		assertEquals(0, allievoTest.getAttivita().size());
	}

	@Test
	public void testEqualsObject() {
		Attivita attivitaIdentica = new Attivita("Attivita di test", LocalDate.of(2018, Month.JANUARY, 4), LocalTime.of(10, 10));
		assertEquals(attivitaIdentica, attivitaTest);
	}

}
