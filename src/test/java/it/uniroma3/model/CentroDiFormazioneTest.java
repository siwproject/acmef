package it.uniroma3.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.model.Allievo.Sesso;

public class CentroDiFormazioneTest {
	
	private CentroDiFormazione centroTest;
	
	private Attivita attivitaTest;
	
	
	@Before
	public void setUp() throws Exception {
		centroTest = new CentroDiFormazione("Centro3", "Via della Vasca Navale 3", "0666456789", 100);
		attivitaTest = new Attivita("Attivita di test", LocalDate.of(2018, Month.JANUARY, 4), LocalTime.of(10, 10));
	}

	@Test
	public void testIscriviAllievo() {
		Allievo allievoMatching = new Allievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, LocalDate.of(2018, Month.JANUARY, 4), "Roma");
		centroTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		Azienda aziendaRiferimento = centroTest.getAzienda();
		assertEquals(1, aziendaRiferimento.getAllievi().size());
		assertEquals(allievoMatching, aziendaRiferimento.getAllievo(allievoMatching.getCodiceStudente()));
	}
	
	@Test
	public void testCreaNuovaAttivita() {
		Attivita attivitaCreata = centroTest.creaNuovaAttivita("Attivita di Test", "06/14/2018 12:00");
		assertEquals(attivitaTest, attivitaCreata);
	}

	@Test
	public void testGetElencoAttivita() {
		Attivita attivitaCreata = centroTest.creaNuovaAttivita("Attivita di Test", "06/14/2018 12:00");
		assertEquals(attivitaTest, attivitaCreata);
		assertEquals(1, centroTest.getElencoAttivita().size());
		assertEquals(attivitaTest, centroTest.getElencoAttivita().get(0));
	}

	@Test
	public void testIscriviAdAttivita() {
		Allievo allievoIscritto = centroTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		Attivita attivitaCreata = centroTest.creaNuovaAttivita("Attivita di Test", "06/14/2018 12:00");
		
		centroTest.iscriviAdAttivita("Attivita di Test", 10L);
		assertEquals(allievoIscritto, attivitaCreata.getAllievi().get(10L));
		assertEquals(attivitaCreata, allievoIscritto.getAttivita().get(0));
	}

	@Test
	public void testDisiscriviAllievo() {
		Allievo allievoIscritto = centroTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		Attivita attivitaCreata = centroTest.creaNuovaAttivita("Attivita di Test", "06/14/2018 12:00");
		
		centroTest.iscriviAdAttivita("Attivita di Test", 10L);
		centroTest.disiscriviAllievo(10L, "Attivita di Test");
		
		assertEquals(0, attivitaCreata.getAllievi().size());
		assertEquals(0, allievoIscritto.getAttivita().size());
	}

	@Test
	public void testElencoAttivitaAllievo() {
		Allievo allievoIscritto = centroTest.iscriviAllievo("Matteo", "Giordano", "MatteoGio96@gmail.com", "3936516619", Sesso.MASCHIO, "10/18/1996", "Roma");
		Attivita attivitaCreata = centroTest.creaNuovaAttivita("Attivita di Test", "06/14/2018 12:00");
		
		centroTest.iscriviAdAttivita("Attivita di Test", 10L);
		assertEquals(allievoIscritto, attivitaCreata.getAllievi().get(10L));
		assertEquals(attivitaCreata, allievoIscritto.getAttivita().get(0));
		
		assertEquals(1, attivitaCreata.getAllievi().size());
	}

}
