package it.uniroma3.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import it.uniroma3.constants.Constants;
import it.uniroma3.model.Allievo.Sesso;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.Synchronized;


@Getter @Setter
@EqualsAndHashCode(exclude={"centri", "allievi"})
@Entity
public class Azienda {

	private static Long codiceAzienda;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String nome;
	
	private static Azienda instance;
	
	@OneToMany(mappedBy="azienda")
	private List<CentroDiFormazione> centri;
	
	@OneToMany
	@JoinColumn(name = "azienda_id")
	private Map<Long, Allievo> allievi;
	
	private Azienda() {
		Azienda.codiceAzienda = Constants.CODICE_AZIENDA;
		this.nome = Constants.NOME_AZIENDA;
		allievi = new HashMap<Long, Allievo>();
	}

	@Synchronized
	public static synchronized Azienda getInstance() {
		if (instance == null) {
			instance = new Azienda();
		}
		return instance;
	}
	
	
	public Allievo iscriviAllievo(String nomeAllievo, String cognomeAllievo, String email, String telefono, Sesso sesso, LocalDate dataNascita, String luogoNascita) {
		Allievo allievo = new Allievo(nomeAllievo, cognomeAllievo, email, telefono, sesso, dataNascita, luogoNascita);
		allievi.put(allievo.getCodiceStudente(), allievo);
		return allievo;
	}
	
	public Allievo iscriviAllievo(Allievo allievo) {
		allievi.put(allievo.getCodiceStudente(), allievo);
		return allievo;
	}
	
	public Allievo getAllievo(Long codiceStudente) {
		return allievi.get(codiceStudente);		
	}
	
	public List<Attivita> getAttivita(Long codiceStudente) {
		Allievo allievo = allievi.get(codiceStudente);
		return allievo.getAttivita();
	}
	
//	public Map<String, Map<String, List<Allievo>>> elencoAllieviPerAttivita(){
//		
//		Map<String, Map<String, List<Allievo>>> report = new HashMap<String, Map<String, List<Allievo>>>();
//		
//		for (CentroDiFormazione centro: centri) {
//			report.put(centro.getNome(), centro.elencoAllieviPerAttivita());
//		}
//		
//		return report;
//	}

	public List<Allievo> getListaAllievi() {
		return new ArrayList<Allievo>(this.allievi.values());
	}
}
