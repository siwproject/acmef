package it.uniroma3.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
@EqualsAndHashCode(exclude={"centro", "allievi"})
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"nome", "centro_id"}))
public class Attivita {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	@NonNull
	private String nome;
	
	public enum Tipologia {
		MATEMATICA,
		SCIENZE,
		ITALIANO,
		INGLESE,
		SPAGNOLO,
		FILOSOFIA,
		INFORMATICA
	}
	
	@Enumerated(EnumType.STRING)
	private Tipologia tipologia;
	
	@Column(nullable = false)
	private String descrizione;
	
	@ManyToOne
	private CentroDiFormazione centro;
	
	
	@Column(nullable = false)
	@DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
	private LocalDate data;

	@Column(nullable = false)
	@DateTimeFormat(iso= DateTimeFormat.ISO.TIME)
	private LocalTime oraInizio;
	
	//@ManyToMany(cascade = CascadeType.ALL)
	//private Map<Long, Allievo> allieviMap;
	
	@ManyToMany(mappedBy = "attivita", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Allievo> allievi;

	public Attivita(String nome, LocalDate data, LocalTime oraInizio) {
		this.nome = nome;
		this.data = data;
		this.oraInizio = oraInizio;
		//this.allieviMap = new HashMap<Long, Allievo>();
		this.allievi = new ArrayList<Allievo>();
	}
	
//	public void aggiungiAllievo(Allievo allievo) {
//		allieviMap.put(allievo.getCodiceStudente(), allievo);
//		allievo.aggiungiAttivita(this);
//	}
//	
//	public void disiscriviAllievo(Long codiceStudente) {
//		Allievo a = allieviMap.get(codiceStudente);
//		allieviMap.remove(a.getCodiceStudente());
//		a.eliminaAttivita(this);
//	}
//	
//	public List<Allievo> getAllAllievi() {
//		return new LinkedList<Allievo>(allieviMap.values());
//	}
	
	public void aggiungiAllievo(Allievo allievo) {
		allievi.add(allievo);
		allievo.aggiungiAttivita(this);
	}
	
	public void disiscriviAllievo(Allievo allievo) {
		allievi.remove(allievo);
		allievo.eliminaAttivita(this);
	}
}
