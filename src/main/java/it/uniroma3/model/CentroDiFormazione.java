package it.uniroma3.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import it.uniroma3.model.Allievo.Sesso;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
@EqualsAndHashCode(exclude={"id", "indirizzo", "telefono", "capienzaMassima", "azienda", "attivita"})
public class CentroDiFormazione {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, unique = true)
	@NonNull
	private String nome;

	@Column(nullable = false)
	@NonNull
	private String indirizzo;

	@Column(nullable = false)
	private String telefono;

	@Column(nullable = false)
	private Integer capienzaMassima;

	@ManyToOne
	private Azienda azienda;

	//@OneToMany(mappedBy = "centro")
	//private Map<String, Attivita> attivitaMap;
	
	@OneToMany(mappedBy = "centro", cascade = CascadeType.ALL)
	private List<Attivita> attivita;

	public CentroDiFormazione(String nome, String indirizzo, String telefono, int capienzaMassima) {
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.telefono = telefono;
		this.capienzaMassima = capienzaMassima;
		this.azienda = Azienda.getInstance();
		//this.attivitaMap = new HashMap<String, Attivita>();
		this.attivita = new ArrayList<Attivita>();
	}	

	public Allievo iscriviAllievo(Allievo allievo) {
		return azienda.iscriviAllievo(allievo.getNome(), allievo.getCognome(), allievo.getEmail(), allievo.getTelefono(), allievo.getSesso(), allievo.getDataNascita(), allievo.getLuogoNascita());
	}
	
	public Allievo iscriviAllievo(String nomeAllievo,String cognomeAllievo, String email, String telefono, Sesso sesso, LocalDate dataNascita, String luogoNascita) {
		return azienda.iscriviAllievo(nomeAllievo, cognomeAllievo, email, telefono, sesso, dataNascita, luogoNascita);
	}

//	public List<Attivita> getElencoAttivita() {
//		Collection<Attivita> attivitaList = attivitaMap.values();
//		List<Attivita> lista = new LinkedList<Attivita>(attivitaList);
//		return lista;
//	}
//
//	public void iscriviAdAttivita(String nomeAttivita, Long codiceStudente) {
//		Allievo allievo = azienda.getAllievo(codiceStudente);
//		Attivita a = attivitaMap.get(nomeAttivita);
//		a.aggiungiAllievo(allievo);
//	}
//
//	public void disiscriviAllievo(Long codiceStudente, String nomeAttivita) {
//		Attivita a = attivitaMap.get(nomeAttivita);
//		a.disiscriviAllievo(codiceStudente);
//	}

	public List<Attivita> elencoAttivitaAllievo(Long codiceStudente) {
		return azienda.getAttivita(codiceStudente); 
	}

	public Attivita creaNuovaAttivita(String nomeAttivita, LocalDate data, LocalTime oraInizio) {
		Attivita a = new Attivita(nomeAttivita, data, oraInizio);
		attivita.add(a);
		a.setCentro(this);
		return a;
	}
	
	public void aggiungiAttivita(Attivita a) {
		attivita.add(a);
		a.setCentro(this);
	}
//
//	public Map<String, List<Allievo>> elencoAllieviPerAttivita() {
//
//		Map<String, List<Allievo>> mappa = new HashMap<String, List<Allievo>>();
//
//		for (Map.Entry<String, Attivita> entry : attivitaMap.entrySet())
//		{
//			mappa.put(entry.getKey(), entry.getValue().getAllAllievi());
//		}
//		return mappa;
//	}
}
