package it.uniroma3.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
@EqualsAndHashCode(exclude={"centro"})
public class Responsabile {
	
	@Id
	private String username;

	@Column(nullable = false)
	@NonNull
	private String nome;
	
	@Column(nullable = false)
	@NonNull
	private String email;

	@Column(nullable = false)
	@NonNull
	private String pwd;
	
	@Column(nullable = false)
	private String ruolo;
	
	@OneToOne
	private CentroDiFormazione centro;

	public Responsabile(String nome, String username, String email, String pwd, String ruolo) {
		this.nome = nome;
		this.username = username;
		this.email = email;
		this.pwd = new BCryptPasswordEncoder().encode(pwd);
		this.ruolo = ruolo;
	}
}
