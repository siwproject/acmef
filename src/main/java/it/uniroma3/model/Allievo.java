package it.uniroma3.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
@EqualsAndHashCode(exclude={"attivita"})
public class Allievo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long codiceStudente;
	
	@Column(nullable = false)
	private String nome;
	
	public enum Sesso {
		MASCHIO, FEMMINA
	}
	
	@Enumerated(EnumType.STRING)
	private Sesso sesso;
	
	@Column(nullable = false)
	private String cognome;
	
	@Column(nullable = false)
	@Email
	private String email;
	
	private String telefono;
	
	@Column(nullable = false)
	@DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
	private LocalDate dataNascita;
	
	@Column(nullable = false)
	@NonNull
	private String luogoNascita;
	
	@OneToOne
	private Azienda azienda;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Attivita> attivita;
	
	public Allievo(String nome, String cognome, String email, String telefono, Sesso sesso, LocalDate dataNascita, String luogoNascita) {
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.sesso = sesso;
		this.telefono = telefono;
		this.dataNascita = dataNascita;
		this.luogoNascita = luogoNascita;
		this.attivita = new ArrayList<>();
		
		//Decommentare per eseguire test su model
		//this.codiceStudente = 10L;
		
		//Decommentare per eseguire test su UseCase
		//this.codiceStudente = new Random().nextLong();
	}
	
	public void aggiungiAttivita(Attivita a) {
		attivita.add(a);
	}
	
	public void eliminaAttivita(Attivita a) {
		attivita.remove(a);
	}
}
