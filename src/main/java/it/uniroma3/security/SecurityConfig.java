package it.uniroma3.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final String usersQuery = "SELECT username,pwd,1 FROM responsabile WHERE username = ?";
	private final String rolesQuery = "SELECT username,ruolo FROM responsabile WHERE username = ?";

	@Qualifier("dataSource")
	@Autowired
	private DataSource dataSource;
	
	@Bean
	public AccessDeniedHandler accessDeniedHandler(){
	    return new CustomAccessDeniedHandler();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
		.passwordEncoder(new BCryptPasswordEncoder())
		.usersByUsernameQuery(usersQuery)
		.authoritiesByUsernameQuery(rolesQuery);
	}

	@Override
	public void configure(WebSecurity web) {
		web
		.ignoring()
		.antMatchers("/static/**", "/css/**", "/images/**", "/js/**", "/vendor/**");
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
		.passwordEncoder(new BCryptPasswordEncoder())
		.usersByUsernameQuery(usersQuery)
		.authoritiesByUsernameQuery(rolesQuery);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable()
		.authorizeRequests()
			.antMatchers("/static/**", "/assets/**","/images/**", "/login").permitAll()
			.anyRequest().authenticated()
			.and()
		.formLogin()
			.usernameParameter("username")
			.passwordParameter("password")
			.failureUrl("/login?error")
			.defaultSuccessUrl("/")
			.loginPage("/login")
    		.permitAll()
    		.and()
		.logout()
			.logoutSuccessUrl("/logout")
			.logoutSuccessUrl("/login?logout");
		//	.and()
		//.exceptionHandling().accessDeniedHandler(accessDeniedHandler());
	}
}


