package it.uniroma3.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.model.Responsabile;

@Repository
public interface ResponsabileRepository extends CrudRepository<Responsabile, Long>{
		
		Responsabile findByUsername(String username);
		Responsabile findByCentro(CentroDiFormazione centro);
		Responsabile findByRuolo(String ruolo);
}