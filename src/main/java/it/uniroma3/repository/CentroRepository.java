package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.model.CentroDiFormazione;

@Repository
public interface CentroRepository extends CrudRepository<CentroDiFormazione, Long>{

	CentroDiFormazione findByNome(String nome);
	List<CentroDiFormazione> findByIndirizzo(String indirizzo);
	List<CentroDiFormazione> findByNomeAndIndirizzo(String nome, String indirizzo);
	List<CentroDiFormazione> findByCapienzaMassima(Integer capacita);
}