package it.uniroma3.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.model.Attivita;
import it.uniroma3.model.CentroDiFormazione;

@Repository
public interface AttivitaRepository extends CrudRepository<Attivita, Long>{
	
	Attivita findByNome(String nome);
	List<Attivita> findByData(LocalDate data);
	List<Attivita> findByOraInizio(LocalTime oraInizio);
	List<Attivita> findByCentroId(Long centroId);
	Attivita findByNomeAndCentro(String nome, CentroDiFormazione centro);
}