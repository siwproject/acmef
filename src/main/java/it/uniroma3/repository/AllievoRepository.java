package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;


public interface AllievoRepository extends CrudRepository<Allievo, Long>{
		
		Allievo findByCodiceStudente(Long codiceStudente);
		List<Allievo> findByNome(String nome);
		List<Allievo> findByCognome(String cognome);
		List<Allievo> findByNomeAndCognomeAndEmail(String nome, String cognome, String email);
}