package it.uniroma3.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Utils {

	public static final Long CODICE_AZIENDA = 0000L;

	public static boolean isValidDateFormat(String inDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}
	
	public static boolean isTimeStampValid(String inputString)
	{ 
	    SimpleDateFormat format = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm");
	    try{
	       format.parse(inputString);
	       return true;
	    }
	    catch(ParseException e)
	    {
	        return false;
	    }
	}

	/**
	 * La ricerca per caratteri è più veloce
	 * 
	 * @param nome
	 * @return true se contiene solo lettere, false altrimenti
	 */
	public static boolean contieneSoloLettere(String nome) {
		char[] caratteri = nome.toCharArray();

		for (char c : caratteri) {
			if(!Character.isLetter(c)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * La ricerca per matching con espressione è più
	 * lenta ma piu leggibile.
	 * 
	 * @param nome
	 * @return true se contiene solo lettere, false altrimenti
	 */
	//	public static boolean contieneSoloLettere(String name) {
	//	    return name.matches("[a-zA-Z]+");
	//	}

}