package it.uniroma3.translator;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import java.sql.Time;
import java.time.LocalTime;

@Converter(autoApply = true)
public class CustomTimeTranslator implements AttributeConverter<LocalTime, Time>{

	@Override
	public Time convertToDatabaseColumn(LocalTime localtime) {
		if (localtime != null) {
			return Time.valueOf(localtime);
		}
		return null;
	}

	@Override
	public LocalTime convertToEntityAttribute(Time timeDB) {
		if (timeDB != null) {
			return timeDB.toLocalTime();
		}
		return null;
	}
}
