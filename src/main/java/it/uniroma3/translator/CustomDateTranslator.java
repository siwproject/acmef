package it.uniroma3.translator;

import javax.persistence.AttributeConverter;

import javax.persistence.Converter;
import java.time.LocalDate;
import java.sql.Date;


@Converter(autoApply = true)
public class CustomDateTranslator implements AttributeConverter<LocalDate, Date> {

	@Override
	public Date convertToDatabaseColumn(LocalDate localdate) {
		if (localdate != null) {
			return Date.valueOf(localdate);
		}
		return null;
	}

	@Override
	public LocalDate convertToEntityAttribute(Date dateDB) {
		if (dateDB != null) {
			return dateDB.toLocalDate();
		}
		return null;
	}

}
