package it.uniroma3.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.model.Attivita;
import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.ResponsabileService;

@Controller
public class CentroController {

	@Autowired
	private CentroService centroService;

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private ResponsabileService responsabileService;


	/**
	 * Mapping per redirect a pagina del centro associata al Responsabile che ha effettuato il login
	 * 
	 * @param centroId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = ("/centro/{centroid}"), method = RequestMethod.GET)
	public String mostraCentro(@PathVariable("centroid") Long centroId, Model model, HttpSession session) {
		String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
		model.addAttribute("centro", this.centroService.findById(centroId));
		session.setAttribute("centro", this.centroService.findById(centroId));
		model.addAttribute("responsabile", this.responsabileService.findByRuolo(centroId.toString()));
		List<Attivita> listaAttivita = this.attivitaService.findByCentroId(centroId);
		model.addAttribute("listaAttivita", listaAttivita);
		return "centro/mainCentro";
	}


	
	/**
	 * Mapping per redirect a pagina di rimozione allievo
	 * 
	 * @param centerId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = ("/delAllievo"))
	public String rimozioneAllievo(Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
        return "redirect:/allievo/rimuoviAllievo";
	}

	
	/**
	 * Mapping per redirect a pagina di aggiunta allievo
	 * 
	 * @param centerId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = ("/addAllievo"))
	public String futuroAllievo(Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
        return "redirect:/allievo/aggiungiAllievo";
	}	
	
	/**
	 * Mapping per redirect a pagina di aggiunta nuova attivita
	 * 
	 * @param centerId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = ("/addAttivita"))
	public String nuovaAttivita(Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
        return "redirect:/attivita/aggiungiAttivita";
	}
}