package it.uniroma3.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.controller.validator.AllievoValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Azienda;
import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;

@Controller
public class AllievoController {
	
	private Azienda azienda = Azienda.getInstance();

	@Autowired
	private AllievoService allievoService;

	@Autowired
	private AllievoValidator validator;
	
	@RequestMapping("/allievi")
	public String allievi(Model model) {
		model.addAttribute("allievi", this.allievoService.findAll());
		return "allieviList";
	}

	@RequestMapping("/scegliAllievoController")
	public String scegliAllievo(Model model, HttpSession session) {
		List<Allievo> listaAllievi = this.allievoService.findAll();
		model.addAttribute("listaAllievi", listaAllievi);
		model.addAttribute("centro", (CentroDiFormazione) session.getAttribute("centro"));
		return "allievo/scegliAllievo";
	}
	
	@RequestMapping("/allievo/rimuoviAllievo")
	public String rimuoviAllievo(Model model, HttpSession session) {
		model.addAttribute("centro", (CentroDiFormazione) session.getAttribute("centro"));
		model.addAttribute("allievo", new Allievo());
		return "allievo/delAllievo";
	}
	
	@RequestMapping(value = ("/allievo/aggiungiAllievo"), method = RequestMethod.GET)
	public String addAllievo(Model model, HttpSession session) {
		model.addAttribute("allievo", new Allievo());
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		return "allievo/addAllievo";
	}
	
	@RequestMapping("/allievo/cerca")
	public String cercaAllievo(@ModelAttribute("allievo") Allievo allievo, Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		
		List<Allievo> listaAllievi = this.allievoService.findByNome(allievo.getNome());
		model.addAttribute("listaAllievi", listaAllievi);
		return "allievo/delAllievo";
	}
	
	
	@RequestMapping("/allievo/infoAllievo/{id}")
	public String showAllievo(@PathVariable("id") Long id, Model model, HttpSession session) {
		Azienda azienda = (Azienda) session.getAttribute("azienda");
		model.addAttribute("azienda", azienda);
		model.addAttribute("allievo", this.allievoService.findById(id));
		return "azienda/infoAllievo";
	}

	@RequestMapping(value = "/allievo", method = RequestMethod.POST)
	public String newAllievo(@Valid @ModelAttribute("allievo") Allievo allievo, 
			Model model, BindingResult bindingResult, HttpSession session) {
		
		String nomeU = allievo.getNome().substring(0, 1).toUpperCase() + allievo.getNome().substring(1);
		String cognomeU = allievo.getCognome().substring(0, 1).toUpperCase() + allievo.getCognome().substring(1);
		
		allievo.setNome(nomeU);
		allievo.setCognome(cognomeU);
		
		this.validator.validate(allievo, bindingResult);
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		
		if (this.allievoService.alreadyExists(allievo)) {
			model.addAttribute("exists", "L'allievo già risulta iscritto");
			return "allievo/addAllievo";
		}

		else {
			if (!bindingResult.hasErrors()) {
				azienda.iscriviAllievo(allievo.getNome(), allievo.getCognome(), allievo.getEmail(), allievo.getTelefono(), allievo.getSesso(), allievo.getDataNascita(), allievo.getLuogoNascita());
				this.allievoService.save(allievo);
				return "allievo/infoAllievo";
			}
		}
		return "allievo/addAllievo";
	}
}
