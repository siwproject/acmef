package it.uniroma3.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.controller.validator.AttivitaValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;

@Controller
public class AttivitaController {

	@Autowired
	private AttivitaValidator validator;

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private AllievoService allievoService;


	@RequestMapping(value = "/attivita/aggiungiAttivita", method = RequestMethod.GET)
	public String aggiungiAttivita(Model model, HttpSession session) {
		model.addAttribute("attivita", new Attivita());
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		return "attivita/newAttivita";
	}
	
	@RequestMapping(value = "/attivita/scegliAttivita/{allievoId}", method = RequestMethod.GET)
	public String scegliAttivita(@PathVariable("allievoId") Long id, Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		
		Allievo allievo = this.allievoService.findById(id);
		List<Attivita> attivitaIscritto = allievo.getAttivita();
		
		model.addAttribute("listaAttivita", attivitaIscritto);
		session.setAttribute("allievo", allievo);
		return "attivita/scegliAttivita";
	}

	@RequestMapping(value = "/infoAttivita/{nomeAttivita}", method = RequestMethod.GET)
	public String aggiungiAllievoAdAttivita(@PathVariable("nomeAttivita") String nomeAttivita, Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		Attivita attivita = this.attivitaService.findByNomeAndCentro(nomeAttivita, centro);
		model.addAttribute("attivita", attivita);
		session.setAttribute("attivita", attivita);

		return "attivita/infoAttivita";
	}
	
	@RequestMapping(value = "/attivita/infoAllievo/{allievoId}", method = RequestMethod.GET)
	public String infoAllievo(@PathVariable("allievoId") Long id, Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		model.addAttribute("allievo", this.allievoService.findById(id));
		return "allievo/infoAllievo";
	}
	
	@RequestMapping(value = "/attivita/disiscrivi/{nomeAttivita}", method = RequestMethod.GET)
	public String disiscriviAllievo(@PathVariable("nomeAttivita") String nomeAttivita, Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		
		Attivita attivita = this.attivitaService.findByNomeAndCentro(nomeAttivita, centro);
		Allievo daRimuovere = (Allievo) session.getAttribute("allievo");
		daRimuovere.eliminaAttivita(attivita);
		attivita.disiscriviAllievo(daRimuovere);
		this.allievoService.update(daRimuovere);
		this.attivitaService.update(attivita);
		
		
		List<Allievo> listaAllievi = attivita.getAllievi();
		model.addAttribute("listaAllievi", listaAllievi);
		model.addAttribute("attivita", attivita);
		
		return "attivita/mostraAllievi";
	}
	
	

	@RequestMapping(value = "/attivita/allieviXAttivita/{allievoId}", method = RequestMethod.GET)
	public String reportAllieviPerAttivita(@PathVariable("allievoId") Long id, Model model, HttpSession session) {
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);
		Attivita attivitaTarget = (Attivita) session.getAttribute("attivita");
		model.addAttribute("attivita", (Attivita) session.getAttribute("attivita"));

		Allievo allievo = this.allievoService.findById(id);

		if (!attivitaTarget.getAllievi().contains(allievo)) {

			attivitaTarget.aggiungiAllievo(allievo);
			this.allievoService.update(allievo);
		}
		model.addAttribute("listaAllievi", attivitaTarget.getAllievi());
		return "attivita/mostraAllievi";
	}

	@RequestMapping(value = "/attivita", method = RequestMethod.POST)
	public String newAllievo(@Valid @ModelAttribute("attivita") Attivita attivita, 
			Model model, BindingResult bindingResult, HttpSession session) {

		this.validator.validate(attivita, bindingResult);
		CentroDiFormazione centro = (CentroDiFormazione) session.getAttribute("centro");
		model.addAttribute("centro", centro);

		if (this.attivitaService.alreadyExists(attivita, centro)) {
			model.addAttribute("exists", "L'attività già esiste");
			return "attivita/newAttivita";
		}

		else {
			if (!bindingResult.hasErrors()) {
				attivita.setCentro(centro);
				this.attivitaService.save(attivita);
				return "redirect:/centro/"+ centro.getId();
			}
		}
		return "attivita/newAttivita";
	}
}
