package it.uniroma3.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.utils.Utils;

@Component
public class AttivitaValidator implements Validator {

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "data", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oraInizio", "required");
		
		// Check su nome
		containsOnlyLetters(o, errors);
		// Check su orario svolgimento nel caso di gestione con String
		//isValidDate(o, errors);
	}
	
	/**
	 * Nel caso di gestione di Data con stringhe si rende necessario questo metodo
	 * 
	 */

//	private void isValidDate(Object o, Errors errors) {
//		Attivita attivita = (Attivita) o;
//		if (!Utils.isTimeStampValid(attivita.getData())){
//			errors.rejectValue("dataOraSvolgimento", "notvalid");
//		}
//	}
	
	private void containsOnlyLetters(Object o, Errors errors) {
		Attivita attivita = (Attivita) o;
		if(!Utils.contieneSoloLettere(attivita.getNome())) {
			errors.rejectValue("nome", "notvalid");
		}
	}
	@Override
	public boolean supports(Class<?> aClass) {
		return Allievo.class.equals(aClass);
	}	
}
