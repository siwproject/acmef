package it.uniroma3.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Allievo;
import it.uniroma3.utils.Utils;

@Component
public class AllievoValidator implements Validator {

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
		
		// Check su nome e cognome
		containsOnlyLetters(o, errors);
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
		
		// Check su numero di telefono
		isPhoneNumber(o, errors);
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dataNascita", "required");
		
		// Check su formato data da decommentare nel caso di gestione tramite String
		//isValidDate(o, errors);
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "luogoNascita", "required");
		
		// Check su luogo di nascita
		containsOnlyLetters(o, errors);
	}


	/**
	 * Nel caso di gestione di Date con stringhe si rende necessario questo metodo
	 * 
	 */
	
//	private void isValidDate(Object o, Errors errors) {
//		Allievo allievo = (Allievo) o;
//		if (!Utils.isValidDateFormat(allievo.getDataNascita())){
//			errors.rejectValue("dataNascita", "notvalid");
//		}
//	}
	
	private void isPhoneNumber(Object o, Errors errors) {
		Allievo allievo = (Allievo) o;
		
		String regEsp = "^(1\\-)?[0-9]{3}\\-?[0-9]{3}\\-?[0-9]{4}$";
		
		if (!allievo.getTelefono().matches("[0-9]+")){
			errors.rejectValue("telefono", "notvalidnumbers");
		}
		if(!allievo.getTelefono().matches(regEsp)) {
			errors.rejectValue("telefono", "notvalid");
		}
		
	}
	
	private void containsOnlyLetters(Object o, Errors errors) {
		Allievo allievo = (Allievo) o;
		if(!Utils.contieneSoloLettere(allievo.getNome())) {
			errors.rejectValue("nome", "notvalid");
		}
		
		if(!Utils.contieneSoloLettere(allievo.getCognome())) {
			errors.rejectValue("cognome", "notvalid");
		}
		
		if(!Utils.contieneSoloLettere(allievo.getLuogoNascita())) {
			errors.rejectValue("luogoNascita", "notvalid");
		}
	}

	
	@Override
	public boolean supports(Class<?> aClass) {
		return Allievo.class.equals(aClass);
	}	
}
