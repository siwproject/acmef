package it.uniroma3.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Azienda;
import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AziendaService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.ResponsabileService;

@Controller
public class AziendaController {

	@Autowired
	private AziendaService aziendaService;
	
	@Autowired
	private CentroService centroService;
	
	@Autowired
	private AllievoService allievoService;

	@RequestMapping(value = ("/azienda/amministrazione"), method = RequestMethod.GET)
	public String mostraAzienda(Model model, HttpSession session) {
		Azienda azienda = this.aziendaService.findByNome("Acme Formazione");
		session.setAttribute("azienda", azienda);
		model.addAttribute("azienda", azienda);
		
		return "azienda/mainAzienda";
	}
	
	@RequestMapping(value = ("/listaCentri"), method = RequestMethod.GET)
	public String listaCentri(Model model, HttpSession session) {
		Azienda azienda = this.aziendaService.findByNome("Acme Formazione");
		List<CentroDiFormazione> centri = this.centroService.findAll();
		model.addAttribute("listaCentri", centri);
		model.addAttribute("azienda", azienda);
		return "azienda/mostraCentri";
	}
	
	@RequestMapping(value = ("/reportAllievi"), method = RequestMethod.GET)
	public String reportAllievi(Model model, HttpSession session) {
		Azienda azienda = this.aziendaService.findByNome("Acme Formazione");
		List<Allievo> allievi =this.allievoService.findAll();
		model.addAttribute("listaAllievi", allievi);
		model.addAttribute("azienda", azienda);
		return "azienda/report";
	}
}