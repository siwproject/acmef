package it.uniroma3.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Model model,
            @RequestParam(value = "logout", required = false) String logout,
            @RequestParam(value = "error", required = false) String error) {
        return "login/login";
    }

    @RequestMapping("/")
    public String reindirizzamento(Model model) {
    	String ruolo = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
    	String ruoloOttenuto = ruolo.substring(1, ruolo.length()-1);
    	if(ruoloOttenuto.equals("ADMIN")) {
    		 return "redirect:/azienda/amministrazione";
    	} else {
    		 return "redirect:/centro/"+ ruoloOttenuto;
    	}
    }

    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public String loginPage() {
        return "accessDenied";
    }
}
