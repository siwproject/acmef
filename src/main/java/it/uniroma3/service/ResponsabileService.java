package it.uniroma3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.model.Responsabile;
import it.uniroma3.repository.ResponsabileRepository;

@Transactional
@Service
public class ResponsabileService {
	
	@Autowired
	private ResponsabileRepository responsabileRepository;
	
	public Responsabile save(Responsabile responsabile) {
		return this.responsabileRepository.save(responsabile);
	}
	
	public Responsabile update(Responsabile responsabile) {
    	return this.responsabileRepository.save(responsabile);
    }
	
	public Responsabile findByCentro(CentroDiFormazione centro) {
    	return this.responsabileRepository.findByCentro(centro);
    }
	
	public Responsabile findByUsername(String username) {
    	return this.responsabileRepository.findByUsername(username);
    }
    
    public Responsabile findByRuolo(String ruolo) {
    	return this.responsabileRepository.findByRuolo(ruolo);
    }
    
    public List<Responsabile> findAll() {
        return (List<Responsabile>) this.responsabileRepository.findAll();
    }
	
}
