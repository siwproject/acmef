package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Attivita;
import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.repository.CentroRepository;

@Transactional
@Service
public class CentroService {

	@Autowired
	private CentroRepository centroRepository;
	
	public CentroDiFormazione save(CentroDiFormazione centro) {
		return this.centroRepository.save(centro);
	}

	public List<CentroDiFormazione> findAll() {
		return (List<CentroDiFormazione>) this.centroRepository.findAll();
	}

	public void deleteByName(String nome) {
		this.centroRepository.delete(this.centroRepository.findByNome(nome));
	}
	
	public void update(CentroDiFormazione centro) {
		this.centroRepository.save(centro);
	}

	public void delete(CentroDiFormazione centro) {
		this.centroRepository.delete(centro);
	}

	public void deleteAllCentri() {
		this.centroRepository.deleteAll();
	}
	
	public List<CentroDiFormazione> findByCapienzaMassima(int capienza) {
		return this.centroRepository.findByCapienzaMassima(capienza);
	}
	
	public void aggiungiAttivita(Attivita attivita, CentroDiFormazione centro) {
		centro.aggiungiAttivita(attivita);
		this.update(centro);
	}
	
	public CentroDiFormazione findById(Long id) {
		Optional<CentroDiFormazione> centro = this.centroRepository.findById(id);
		if (centro.isPresent()) 
			return centro.get();
		else
			return null;
	}

	public boolean alreadyExists(CentroDiFormazione centro) {
		List<CentroDiFormazione> centri = this.centroRepository.findByNomeAndIndirizzo(centro.getNome(), centro.getIndirizzo());
		if (centri.size() > 0)
			return true;
		else 
			return false;
	}
}
