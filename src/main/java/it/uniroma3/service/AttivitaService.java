package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.repository.AttivitaRepository;

@Transactional
@Service
public class AttivitaService {

	
	@Autowired
	private AttivitaRepository attivitaRepository;
	
	public Attivita save(Attivita attivita) {
		return this.attivitaRepository.save(attivita);
	}
	
	public void update(Attivita attivita) {
		this.attivitaRepository.save(attivita);
	}

	public void delete(Attivita attivita) {
		this.attivitaRepository.delete(attivita);
	}
	
	public void deleteAllActivities(Attivita attivita) {
		this.attivitaRepository.deleteAll();
	}

	public List<Attivita> findAll() {
		return (List<Attivita>) this.attivitaRepository.findAll();
	}
	
	public List<Attivita> findByCentroId(Long centro){
		return this.attivitaRepository.findByCentroId(centro);
	}
	
	public Attivita findByNomeAndCentro(String nome, CentroDiFormazione centro){
		return this.attivitaRepository.findByNomeAndCentro(nome, centro);
	}
	
	public void aggiungiAllievo(Allievo allievo, Attivita attivita) {
		attivita.aggiungiAllievo(allievo);
		this.update(attivita);
	}
	
	public Attivita findById(Long id) {
		Optional<Attivita> attivita = this.attivitaRepository.findById(id);
		if (attivita.isPresent()) 
			return attivita.get();
		else
			return null;
	}
	
	public Attivita findByNome(String nome) {
		return this.attivitaRepository.findByNome(nome);
	}

	public boolean alreadyExists(Attivita attivita, CentroDiFormazione centro) {
		Attivita attivitaEventuale = this.attivitaRepository.findByNomeAndCentro(attivita.getNome(), centro);
		if (attivitaEventuale != null)
			return true;
		else 
			return false;
	}
}
