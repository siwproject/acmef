package it.uniroma3;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Allievo.Sesso;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Azienda;
import it.uniroma3.model.CentroDiFormazione;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.AziendaService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.ResponsabileService;

@SpringBootApplication
public class AcmeFApplication {

	@Autowired
	private CentroService centroService;

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private ResponsabileService responsabileService;

	@Autowired
	private AziendaService aziendaService;
	
	@Autowired
	private AllievoService allievoService;

	public static void main(String[] args) {
		SpringApplication.run(AcmeFApplication.class, args);
	}

	@PostConstruct
	public void init() {

		// --- AZIENDA di contesto --- \\
		Azienda.getInstance();
		this.aziendaService.save(Azienda.getInstance());

		// -- CENTRI DI FORMAZIONE -- \\
		CentroDiFormazione centro1 = new CentroDiFormazione("Centro1", "Via della Vasca Navale 1", "0666456789", 200);
		CentroDiFormazione centro2 = new CentroDiFormazione("Centro2", "Via della Vasca Navale 2", "0666456789", 300);
 		CentroDiFormazione centro3 = new CentroDiFormazione("Centro3", "Via della Vasca Navale 3", "0666456789", 100);
 		this.centroService.save(centro1);
		this.centroService.save(centro2);
		this.centroService.save(centro3);

		// -- RESPONSABILI --- \\
		Responsabile responsabile1 = new Responsabile("Matteo", "user2", "email2@email.com", "password2", centro1.getId().toString());
		Responsabile responsabile2 = new Responsabile("Luca", "user3", "email3@email.com", "password3", centro2.getId().toString());
		Responsabile responsabile3 = new Responsabile("Valerio", "user4", "email4@email.com", "password4", centro3.getId().toString());
		
		// -- DIRETTORE -- \\
		Responsabile direttore = new Responsabile("Paolo Merialdo", "admin", "nonèunpaesepervecchi@email.com", "admin", "ADMIN");
		
		this.responsabileService.save(responsabile1);
		this.responsabileService.save(responsabile2);
		this.responsabileService.save(responsabile3);
		this.responsabileService.save(direttore);
		
		responsabile1.setCentro(centro1);
		responsabile2.setCentro(centro2);
		responsabile3.setCentro(centro3);
		this.responsabileService.update(responsabile1);
		this.responsabileService.update(responsabile2);
		this.responsabileService.update(responsabile3);

		// -- COMPLETAMENTO AZIENDA -- \\
		List<CentroDiFormazione> listaCentri = new LinkedList<CentroDiFormazione>();
		listaCentri.add(centro1);
		listaCentri.add(centro2);
		listaCentri.add(centro3);
		Azienda.getInstance().setCentri(listaCentri);
		this.aziendaService.update(Azienda.getInstance());

		// -- Prima attivita di supporto -- \\

//		Attivita attivita = new Attivita("Attivita di Test", LocalDate.of(2018, Month.JUNE, 20), LocalTime.of(20, 00));
//		String descrizione = "Questa attivita è una attività di test per provare il funzionamento dei controller a le pagine HTML e CSS";
//		attivita.setDescrizione(descrizione);
//		this.attivitaService.save(attivita);
//		centro1.aggiungiAttivita(attivita);
//		this.attivitaService.update(attivita);
//		this.centroService.update(centro1);
		
		
		// -- ALLIEVI -- \\
//		Allievo allievo1 = new Allievo("Matteo", "Giordano", "email@email.com", "3938475869", Sesso.MASCHIO, LocalDate.of(1996, Month.JUNE, 4), "Roma");
//		Allievo allievo2 = new Allievo("Luca", "Carloni", "email@email.com", "3938475869", Sesso.MASCHIO, LocalDate.of(1996, Month.JUNE, 4), "Roma");
//		this.allievoService.save(allievo1);
//		this.allievoService.save(allievo2);
}
}
